/*
 MiniLang: A simple language for learning about creating own programming language.
*/

package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	var args = os.Args
	// remove first go executable file
	filteredArgs := args[1:]

	// Must contains only one file name
	if len(filteredArgs) > 1 {
		panic("Too much arguments. Only Expected main file!")
	}

	// Extracting file name
	var fileName string = filteredArgs[0]

	// File extension mustbe .mini
	validExtension := strings.EqualFold(fileName, "main.mini")

	// Only main.mini file should be passed in args file
	if validExtension == false {
		panic("ERROR: Minilang only start executing from main.mini file. Please correctly locate main.mini file")
	}

	codeBytes, err := os.ReadFile(fileName)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	lxr := NewLexer(string(codeBytes))


	parser := NewParser(lxr)
	ast := parser.plumin()
    fmt.Println(string(codeBytes))
	fmt.Printf("%+v\n", ast)

}
