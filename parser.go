/*
Parser will parse all the tokens and returns the AST
Parser -> Abstruct Syntax Tree(AST)
*/

package main

type ASTNode interface{}

type BinOp struct {
	left     ASTNode
	operator Token
	right    ASTNode
}

type Parser struct {
	lexer        *Lexer
	currentToken Token
}

type Number struct {
	Value int
	Token Token
}

// Create New Parser
func NewParser(l *Lexer) *Parser {
	return &Parser{
		lexer:        l,
		currentToken: l.GetNextToken(),
	}
}

// Implement Consume Token
func (p *Parser) ConsumeToken(t TokenType) {
	if p.currentToken.Type == t {
		p.currentToken = p.lexer.GetNextToken()
	} else {

		panic("ERROR: Invalid syntax match at ConsumeToken")
	}
}

// Implement parseFactor to return the Smallest Unit of Grammer
func (p *Parser) ParseFactor() ASTNode {
	token := p.currentToken
	if token.Type == INTEGER {
		p.ConsumeToken(INTEGER)
		return &Number{
			Value: atoi(token.Value),
			Token: token,
		}
	} else {

		panic("ERROR: Invalid Type to parse INTEGER in ParseFactor")
	}
}

// String to Integer
func atoi(v string) int {
	// From "123" to 123
	output := 0
	for _, value := range v {
		output += output*10 + int(value-'0')
	}
	return output
}

// Implement  parse for Multiply and Division
func (p *Parser) muldiv() ASTNode {
	node := p.ParseFactor()
	for p.currentToken.Type == MUL || p.currentToken.Type == DIV {
		token := p.currentToken
		if token.Type == MUL {
			p.ConsumeToken(MUL)
		}
		if token.Type == DIV {
			p.ConsumeToken(DIV)
		}
		node = BinOp{
			left:     node,
			operator: token,
			right:    p.ParseFactor(),
		}
	}
	return node
}

func (p *Parser) plumin() ASTNode {
	node := p.muldiv()
	for p.currentToken.Type == PLUS || p.currentToken.Type == MINUS {
		token := p.currentToken
		if token.Type == PLUS {
			p.ConsumeToken(PLUS)
		}
		if token.Type == MINUS {
			p.ConsumeToken(MINUS)
		}
		node = BinOp{
			left:     node,
			operator: token,
			right:    p.ParseFactor(),
		}
	}
	return node
}
