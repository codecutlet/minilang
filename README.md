# MiniLang

This is an educational project for me (Debanjan Tewary) to learn how to create a basic programming language.

#### Progress

- [x] Full Lexer implementation
  - [x] Create Token Type & Lexer Type
  - [x] `NewLexer()` function that returns a Lexer of \*Lexer type
  - [x] Implement `Move()`, `SkipWhiteSpace()`,`Integer()`, `String()`, `GetNextToken`()
  - [x] Print all the tokens from input
- [ ] Full Parser implementation
  - [ ] Define AST Node Types
  - [ ] Impl `ConsumeToken()` : cosnume a token & move to next token if type matches
  - [ ] Impl `parseFactor()`: returns the smallest unit from the TokenType grammer
  - [ ] Impl `parseTerm()`: evaluate the expression and returns a binary operation node BinOp for MULTIPLY & DIVISION
  - [ ] Impl `parseExpression()`: evaluate the expression and returns a BinOp for PLUS & MINUS
- [ ] Full Interpreter implementation
- [ ] Maybe Compiler implementation
