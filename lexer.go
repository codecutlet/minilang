package main

import (
	"fmt"
	"unicode"
	"unicode/utf8"
)

type TokenType int

// Declare type of tokens and initialize it with a int value using iota
// iota in go initialize a variable with int value 0
// and subcequent variable gets increamented value
const (
	INTEGER TokenType = iota + 1
	STRING
	PLUS
	MINUS
	MUL
	DIV
	EOF
)

/*
Every Lexer character iteration will return a token
ex: expression 1 + 2
token at 1: &Token{Type:0,Value:"1",}
*/
type Token struct {
	Type  TokenType
	Value string
}

// Generate New token
func NewToken(t TokenType, v string) Token {
	return Token{
		Type:  t,
		Value: v,
	}
}

/*
A Lexer will iterate thought every single character in a file and
it will return a Token{} for every character (currently except whitespace)
if char = 0 then its end of file
*/
type Lexer struct {
	text string
	pos  int
	char rune
}

/*
Move() will move the lexer to next character in text
*/
func (l *Lexer) Move() {
	// Go to next position in text
	l.pos++
	if l.pos > len(l.text)-1 {
		l.char = 0 // EOF
	} else {
		// Decode text from l.pos
		r, _ := utf8.DecodeRuneInString(l.text[l.pos:])
		// DecodeRuneInString returns the first char as rune
		l.char = r
	}
}

/*
SkipWhiteSpace() will not generate Token for white spaces
*/
func (l *Lexer) SkipWhiteSpace() {

	if unicode.IsSpace(l.char) == true {
		l.Move()
	}

	/*
		for unicode.IsSpace(l.char) {
			l.Move()
		}
	*/
}

func (l *Lexer) String() string {
	output := ""
	for unicode.IsLetter(l.char) {
		output += string(l.char)
		l.Move()
	}
	return output
}
func (l *Lexer) Integer() string {
	output := ""
	for unicode.IsDigit(l.char) {
		output += string(l.char)
		l.Move()
	}
	return output
}

// Get the Token For next char
func (l *Lexer) GetNextToken() Token {
	for l.char != 0 {
		// For Integers
		if unicode.IsDigit(l.char) == true {
			intOutput := l.Integer()
			return NewToken(INTEGER, intOutput)

		}

		//Skips white Spaces
		if unicode.IsSpace(l.char) {
			l.SkipWhiteSpace()
			continue
		}

		if unicode.IsLetter(l.char) {
			return NewToken(STRING, l.String())
		}

		//Minus
		if l.char == '-' {
			l.Move()
			return NewToken(MINUS, "-")
		}

		//Plus
		if l.char == '+' {
			l.Move()
			return NewToken(PLUS, "+")
		}
		//Multiplie
		if l.char == '*' {
			l.Move()
			return NewToken(MUL, "*")
		}
		//Division
		if l.char == '/' {
			l.Move()
			return NewToken(DIV, "/")
		}

		panic(fmt.Sprintf("ERROR: Invalid syntax at POS: %v, VALUE: %v", l.pos, string(l.char)))
	}
	// If l.char == 0 then its the end of file
	fmt.Println("EOF at:", l.pos)
	return NewToken(EOF, "")
}

func NewLexer(text string) *Lexer {
	r, _ := utf8.DecodeRuneInString(text)
	// Return the first character as initial lexer
	return &Lexer{
		text: text,
		pos:  0,
		char: r,
	}
}
